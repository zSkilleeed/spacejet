﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeManager : MonoBehaviour {

	public static FadeManager instance;

	public Image FadeImage;
	
	public Color startColor;
	public Color endColor;
	public float duration;


	private void Awake() {
		instance = this;
	}


	private void Start()
	{
		
		FadeIn();
		
	}

	public void FadeIn() {
		StartCoroutine(BeginFade());
	}

	private IEnumerator BeginFade()
	{

		float timer = 0f;
		while(timer <= duration)
		{
			FadeImage.color = Color.Lerp(startColor, endColor, timer / duration);
			timer += Time.deltaTime;
			yield return null;
		}
	}

}
