﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;



public class player_movement : MonoBehaviour
{
    //Private 
    private float playerSetJump;
    private float gravity;
    private float playerSetSpeed;
    private float playerSpeed;
    private float playerJump;
    private float posx;
    public Text fueltext;
    public Text fueltext2;
    public Text lifetext;
    public Text lifetext2;
    public GameObject checkpointText;
    private float posy;
    private bool canMove;
    private int counter;
    private float fuel;
    private float rotation;
    private bool bothKeyDown;
    private float playerRespawnX;
    private float playerRespawnY;
    private int life;
    Rigidbody2D m_Rigidbody;
    Animator animator;
    AudioSource jetpackSound;
    

    //Start Event
    void Start()
    {
        playerRespawnX = transform.position.x;
        playerRespawnY = transform.position.y;
        counter = 0;
        canMove = true;
        life = 3;
        fuel = 100;
        gravity = -3.3f;
        playerSetJump = 3;
        rotation = 0;
        m_Rigidbody = GetComponent<Rigidbody2D>(); //Gets the rigidbody2d component from 'Player'
        m_Rigidbody.freezeRotation = true;
        animator = GetComponent<Animator>();
        jetpackSound = GetComponent<AudioSource>();
        animator.SetBool("Has_Fuel", true);
        checkpointText.SetActive(false);

        
    }

    //Update Event
    void Update()
    {
        //Print Fuel Text
        printText();

        posx = transform.position.x;
        posy = transform.position.y;

        posx += playerSpeed * Time.deltaTime;
        posy += playerJump * Time.deltaTime;
        Physics2D.gravity = new Vector2(0, gravity);

        //Inputs
        bool keyDown_A = Input.GetKey(KeyCode.A);
        bool keyDown_D = Input.GetKey(KeyCode.D);
        bool keyDown_F = Input.GetKey(KeyCode.F);

        //When 'A' is down
        if(canMove){
        if (keyDown_A)
        {
            
            if(!keyDown_D){
                fuel -= 0.10f;
            }
            playerJump = playerSetJump;
            if (fuel > 0)
            {
                if (!jetpackSound.isPlaying)
                {
                    jetpackSound.Play();
                }
                animator.SetBool("Flying", true);
                playerSpeed -= 0.09f;
                if (playerSpeed < -15)
                {
                    playerSpeed = -15;
                }
                if (rotation < 10)
                {
                    rotation += 0.5f;
                    transform.Rotate(new Vector3(0, 0, 1));
                }
                animator.SetBool("Has_Fuel", true);
                Physics2D.gravity = new Vector2(0, 0); //No gravity
                transform.SetPositionAndRotation(new Vector2(posx, posy), transform.rotation);
            }
            else
            {
                animator.SetBool("Has_Fuel", false);    
                fuel = 0;
                keyDown_A = false;
                playerJump = 0;

            }
            }
        }
        //When 'A' is released
        if(canMove){
        if (!keyDown_A)
        {
             if (!keyDown_D)
            {
                animator.SetBool("Flying", false);
                if (jetpackSound.isPlaying)
                {
                    jetpackSound.Stop();
                }

            }
            if (playerSpeed < 0)
            {
                if (playerSpeed <= -2)
                {
                    playerSpeed += 0.03f;
                }
                if (playerSpeed > 0)
                {
                    playerSpeed = 0;
                }
                transform.SetPositionAndRotation(new Vector2(posx, posy), transform.rotation);
                Physics2D.gravity = new Vector2(0, gravity); //Reset gravity
            }
        }
        }

        //When 'D' is down
        if(canMove){
        if (keyDown_D)
        {
         
            if(!keyDown_A){
                fuel -= 0.10f;
            }
            playerJump = playerSetJump;
            if (fuel > 0)
            {
                if (!jetpackSound.isPlaying)
                {
                    jetpackSound.Play();
                }
                animator.SetBool("Flying", true);
                if (playerSpeed <= 15)
                {
                    playerSpeed += 0.09f;
                }
                if (playerSpeed >= 15)
                {
                    playerSpeed = 15;
                }
                if (rotation > -10)
                {
                    rotation -= 0.5F;
                    transform.Rotate(new Vector3(0, 0, -1));
                }
                animator.SetBool("Has_Fuel", true);
                Physics2D.gravity = new Vector2(0, 0); //No gravity
                transform.SetPositionAndRotation(new Vector2(posx, posy), transform.rotation);
            }
            else
            {
                fuel = 0;
                keyDown_D = false;
                playerJump = 0;
                animator.SetBool("Has_Fuel", false);

            }
            }
        }

        //When 'D' is released
        if(canMove){
        if (!keyDown_D)
        {
            if (!keyDown_A)
            {
                animator.SetBool("Flying", false);
                if (jetpackSound.isPlaying)
                {
                    jetpackSound.Stop();
                }
            }
            
            if (playerSpeed > 0)
            {
                if (playerSpeed >= 2)
                {
                    playerSpeed -= 0.03f;
                }
                transform.SetPositionAndRotation(new Vector2(posx, posy), transform.rotation);
                Physics2D.gravity = new Vector2(0, gravity); //Reset gravity
            }
        }
        }

        //When 'A' and 'D' are down
        if (Input.GetKey(KeyCode.A) && Input.GetKey(KeyCode.D))
        {
            if(canMove){
            if (fuel > 0)
            {
                bothKeyDown = true;
                if(bothKeyDown){
                fuel -= 0.10f;
                }
                animator.SetBool("Flying", true);
                if (playerSpeed < 0)
                {

                    if (playerSpeed <= 0)
                    {
                        playerSpeed += 0.05f;
                    }
                    if (playerSpeed == 0)
                    {
                        playerSpeed = 0;
                    }

                    Physics2D.gravity = new Vector2(0, 3); //Low gravity

                }
                if (playerSpeed > 0)
                {
                    if (playerSpeed >= 0)
                    {
                        playerSpeed -= 0.05f;
                    }
                    if (playerSpeed == 0)
                    {
                        playerSpeed = 0;
                    }

                    Physics2D.gravity = new Vector2(0, 3); //Low gravity

                }
                rotation = 0;
                transform.SetPositionAndRotation(new Vector2(posx, posy), new Quaternion(0, 0, 0, 0));
            }
        }else{
            bothKeyDown = false;
        }
        }
        if(life <= 0) {
            Invoke("finalDead", 1.3F);
        } 

    }


    //Colision Event
    
    void OnCollisionEnter2D(Collision2D col)
    {

        //Normal Platform
        if (col.gameObject.tag == "Ground")
        {
            if(jetpackSound.isPlaying){
                jetpackSound.Stop();
            }
            if(fuel <= 0){
                life -= 1;
                Invoke("playerRespawn", 1.4F);
            }
            playerSpeed = 0;
            playerJump = 0;
            rotation = 0;
            animator.SetBool("Flying", false);
            if(fuel < 0){
                animator.SetBool("Has_Fuel", true);
            }

            transform.SetPositionAndRotation(new Vector2(posx, posy), new Quaternion(0, 0, 0, 0));
        }
        //Checkpoint
        if (col.gameObject.tag == "Checkpoint")
        {

            if(jetpackSound.isPlaying){
                jetpackSound.Stop();
            }
            playerRespawnX = transform.position.x;
            playerRespawnY = transform.position.y;
            playerSpeed = 0;
            playerJump = 0;
            rotation = 0;
            fuel = 100;
            animator.SetBool("Flying", false);
            animator.SetBool("Has_Fuel", true);
            transform.SetPositionAndRotation(new Vector2(playerRespawnX, playerRespawnY), new Quaternion(0, 0, 0, 0));
            checkpointText.SetActive(true);
            Invoke("checkpointActive", 2);
        }
        if (col.gameObject.tag == "Finish")
        {
            if(jetpackSound.isPlaying){
                jetpackSound.Stop();
            }
            playerRespawnX = transform.position.x;
            playerRespawnY = transform.position.y;
            playerSpeed = 0;
            playerJump = 0;
            rotation = 0;
            fuel = 100;
            animator.SetBool("Flying", false);
            animator.SetBool("Has_Fuel", true);
            transform.SetPositionAndRotation(new Vector2(posx, posy), new Quaternion(0, 0, 0, 0));
            finish();
        }
        if(col.gameObject.tag == "Dead"){
            if(jetpackSound.isPlaying){
                jetpackSound.Stop();
            }
                if(life > 0){
                    counter++;
                    animator.SetBool("dead", true);
                    if(counter == 1){
                        canMove = false;
                        life -= 1;
                        Invoke("playerRespawn", 1.4F);
                    }

                
            }
        }
    }
    void playerRespawn(){
                
                fuel = 100;
                animator.SetBool("Flying", false);
                animator.SetBool("Has_Fuel", true);
                canMove = true;
                transform.SetPositionAndRotation(new Vector2(playerRespawnX, playerRespawnY), new Quaternion(0, 0, 0, 0));
                playerSpeed = 0;
                playerJump = 0;
                rotation = 0;
                animator.SetBool("dead", false);
                counter = 0;
    }
    void printText()
    {
        fueltext.text = "Fuel " + Mathf.RoundToInt(fuel) + "%";
        fueltext2.text = "Fuel " + Mathf.RoundToInt(fuel) + "%";
        lifetext.text = "Life: " + life;
        lifetext2.text = "Life: " + life;


    }
    void finalDead(){
        SceneManager.LoadScene("dead_");
    }
    void finish(){
        SceneManager.LoadScene("win");
    }

    void checkpointActive(){
        checkpointText.SetActive(false);
    }

     
}
