﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallax_Code : MonoBehaviour {


	private Transform myTransform;
	private Transform backgroundA;
	private Transform backgroundB;


	void Start () {
		myTransform = GetComponent <Transform> ();
		GameObject fondoA = GameObject.FindWithTag("tercero");
		backgroundA = fondoA.GetComponent<Transform> ();
		GameObject fondoB = GameObject.FindWithTag("primero");
		backgroundB = fondoB.GetComponent<Transform> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey (KeyCode.A))
		{
			myTransform.Translate(new Vector3 (-1, 0, 0));
			backgroundA.Translate(new Vector3 (2, 0, 0));
			backgroundB.Translate(new Vector3 (0.1f, 0, 0));
		}
		if (Input.GetKey (KeyCode.D))
		{
			myTransform.Translate(new Vector3 (1, 0, 0));
			backgroundA.Translate(new Vector3 (-2, 0, 0));
			backgroundB.Translate(new Vector3 (-0.1f, 0, 0));
		}
	}
}
